---
title: Home
---
## Welcome to Asif Currimbhoy's Website. 

Here you'll find everything related to Asif Currimbhoy. His life, works, ideas and legacy. 

{{< figure src="https://literaryocean.com/wp-content/uploads/2023/08/Asif-Currimbhoy-1928-%E2%80%93-2008.png)" height="auto" width="300" >}}
